﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour
{
    public AudioSource audioSource; // Drag and drop the AudioSource here in the Inspector
    public AudioClip audioClip; // Assign the audio clip here in the Inspector

    private Button button;

    void Start()
    {
        button = GetComponent<Button>();

        // Ensure the AudioSource is attached
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();

        // Assign the button click listener
        button.onClick.AddListener(PlayAudio);
    }

    public void PlayAudio()
    {
        if (audioSource != null && audioClip != null)
        {
            // Assign the audio clip and play
            audioSource.clip = audioClip;
            audioSource.Play();
        }
        else
        {
            Debug.LogWarning("AudioSource or AudioClip not assigned.");
        }
    }
}
