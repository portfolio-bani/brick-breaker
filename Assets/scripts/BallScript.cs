﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallScript : MonoBehaviour
{
    public Rigidbody2D rigidbody;
    public bool InPlay;
    public Transform paddle;
    public float speed;
    public Transform explosion;
    public Transform powerup;
    AudioSource audio;

    public GameManager manager;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        audio = GetComponent<AudioSource>();

    }


    void Update()
    {
        if (manager.gameOver)
        {
            return;
        }
        if (!InPlay)
        {
            transform.position = paddle.position;
        }
        if (Input.GetButtonDown("Jump") && !InPlay)
        {
            InPlay = true;
            rigidbody.AddForce(Vector2.up * speed);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("bottom"))
        {
            Debug.Log("viola hfhfhfhf");
            rigidbody.velocity = Vector2.zero;
            InPlay = false;
            manager.UpdateLives(-1);
        }


    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("brick"))
        {
            BrickScript brickScript = other.gameObject.GetComponent<BrickScript>();
            if (brickScript.hitsToBreak > 1)
            {
                brickScript.BreakBrick();
            }
            else
            {


                int randChance = Random.Range(1, 101);
                if (randChance < 50)
                {
                    Instantiate(powerup, other.transform.position, other.transform.rotation);
                }
                Transform newExplosion = Instantiate(explosion, other.transform.position, other.transform.rotation);
               // newExplosion.GetComponent<ParticleSystem>().startColor = brickScript.brickColor;
                Destroy(newExplosion.gameObject, 2.5f);


                manager.UpdateScore(brickScript.point);
                manager.UpdateNoOfBricks();
                Destroy(other.gameObject);

            }
            audio.Play();
        }


    }


}
