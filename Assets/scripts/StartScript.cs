﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScript : MonoBehaviour
{
   public void QuitGame()
    {
        Debug.Log("Quit button pushed");
        Application.Quit();
       
    }
    public void StartGame()
    {
        SceneManager.LoadScene("main");
    }
}
