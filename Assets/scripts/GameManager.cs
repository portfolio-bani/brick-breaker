﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int lives;
    public int score;
    public Text liveText;
    public Text scoreText;
    public Text highScoreText;
    public bool gameOver;
    public GameObject gameOverPanel;
    public GameObject LoadLevelPanel;
    public int NoOfBricks;
    public Transform[] levels;
    public int currentLevelIndex = 0;
    void Start()
    {
        liveText.text = "Lives: " + lives;
        scoreText.text = "Score: " + score;
        NoOfBricks = GameObject.FindGameObjectsWithTag("brick").Length;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateLives(int ChangeInLives)
    {
        lives += ChangeInLives;

        //have to check here that no life is left and have to trigger end off the game\\\
        if (lives <= 0)
        {
            lives = 0;
            GameOver();
        }

        liveText.text = "Lives:"+lives;

    }

    public void UpdateScore (int points)
    {
        score += points;
        scoreText.text = "Score: " + score;

    }

    public void UpdateNoOfBricks()
    {
        NoOfBricks--;
        if(NoOfBricks <= 0)
        {
            if (currentLevelIndex >= levels.Length - 1)
            {
                GameOver();
            }
            else
            {
                LoadLevelPanel.SetActive(true);
                LoadLevelPanel.GetComponentInChildren<Text>().text = "Loading Level: " + (currentLevelIndex + 2);
                gameOver = true;
                Invoke("LoadLevel", 3f);
            }
           
        }
    }
    void LoadLevel()
    {
        currentLevelIndex++;
        Instantiate(levels[currentLevelIndex], Vector2.zero, Quaternion.identity);
        NoOfBricks = GameObject.FindGameObjectsWithTag("brick").Length;
        gameOver = false;
        LoadLevelPanel.SetActive(false);
    }

    void GameOver()
    {
        gameOver = true;
        gameOverPanel.SetActive(true);
        ///for showing high score using playerprefs///
        int highScore = PlayerPrefs.GetInt("HIGHTSCORE");
        if (score> highScore)
        {
            PlayerPrefs.SetInt("HIGHTSCORE", score);

            highScoreText.text = "New High Score: " + score;
        }
        else
        {
            highScoreText.text = "Your Score: " + score +"\nHigh Score:"+ highScore;
        }
    }
    public void PlayAgain()
    {
        SceneManager.LoadScene("Main");
    }
    public void Quit() {
        SceneManager.LoadScene("Start menu");
        
    }
}
