﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleScript : MonoBehaviour
{
    public float speed;
    public float leftEdge;
    public float rightEdge;
    public GameManager manager;
    
    void Start()
    {
        
    }

    
    void Update()
    {
        if (manager.gameOver)
        {
            return;
        }
        float horizontal = Input.GetAxis("Horizontal");

        transform.Translate(Vector2.right*horizontal*Time.deltaTime*speed);
        if (transform.position.x < leftEdge)
        {
            transform.position= new Vector2 (leftEdge,transform.position.y);
        }

        if (transform.position.x > rightEdge)
        {
            transform.position = new Vector2(rightEdge, transform.position.y);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
       if (other.CompareTag("extraLife"))
        {
            manager.UpdateLives(1);
            Destroy(other.gameObject);

        }
        //Debug.Log("hit:" + other.name);
    }
}
